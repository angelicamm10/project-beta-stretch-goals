from django.shortcuts import render
from django.http import JsonResponse
import json
from .models import Salesperson, Customer, Sale, AutomobileVO
from django.views.decorators.http import require_http_methods
from .encoders import ( 
    SalesPeopleListEncoder, 
    SalesPersonDetailEncoder, 
    CustomerDetailEncoder, 
    CustomerListEncoder, 
    SaleDetailEncoder, 
    SaleListEncoder)


@require_http_methods(["GET", "POST"])
def api_list_salespeople(request):
    if request.method == "GET":
        try:
            salespeople = Salesperson.objects.all()
            return JsonResponse(
                {"salespeople": salespeople},
                encoder = SalesPeopleListEncoder,
            )
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Does not exist"},
                status = 404
            )
    else:
        try: 
            content = json.loads(request.body)
            salespeople = Salesperson.objects.create(**content)
            return JsonResponse(
                salespeople,
                encoder=SalesPersonDetailEncoder,
                safe=False,
            )
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Could not create salesperson"},
                status = 400
            )


@require_http_methods(["GET", "DELETE"])
def api_show_salesperson(request,id):
    if request.method == "GET":
        try:
            salesperson = Salesperson.objects.get(id=id)
            return JsonResponse(
                salesperson,
                encoder=SalesPersonDetailEncoder,
                safe=False,
            )
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Does not exist"},
                status = 404
            )
    else:
        count, _ = Salesperson.objects.filter(id=id).delete()
        if count > 0: 
            return JsonResponse({"deleted": count > 0})
        else:
            return JsonResponse(
                {"message": "Does not exist"},
                status = 404,
            )


@require_http_methods(["GET", "POST"])
def api_list_customers(request):
    if request.method == "GET":
        try:
            customers = Customer.objects.all()
            return JsonResponse(
                {"customers": customers},
                encoder=CustomerListEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Does not exist"},
                status = 404
            )
    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerDetailEncoder,
                safe=False
            )
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "could not create a customer"},
                status = 400
            )


@require_http_methods(["GET", "DELETE"])
def api_show_customer(request,id):
    if request.method == "GET":
        try:
            customer = Customer.objects.get(id=id)
            return JsonResponse(
                customer,
                encoder=CustomerDetailEncoder,
                safe=False
            )
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Does not exist"},
                status = 404
            )
    else:
        
        count, _ = Customer.objects.filter(id=id).delete()
        if count > 0:
            return JsonResponse({"deleted": count > 0})
        else:
            return JsonResponse(
                {"message": "Does not exist"},
                status = 404,
            )


@require_http_methods(["GET","POST"])
def api_list_sales(request, auto_vo_id=None):
    if request.method == "GET":
        try:
            if auto_vo_id is not None:
                sales = Sale.objects.filter(autos=auto_vo_id)
            else:
                sales = Sale.objects.all()
            return JsonResponse(
                {"sales": sales},
                encoder=SaleListEncoder,
            )
        except Sale.DoesNotExist:
            return JsonResponse(
                {"message": "Does not exist"},
                status = 404
            )
    else:
        try:
            content = json.loads(request.body)
            auto_vin = content["automobile"]
            auto = AutomobileVO.objects.get(vin=auto_vin)
            content["automobile"] = auto

            employee_id = content["salesperson"]
            salesperson = Salesperson.objects.get(employee_id=employee_id)
            content["salesperson"] = salesperson

            customer_name = content["customer"]
            customer = Customer.objects.get(id=customer_name)
            content["customer"] = customer

            sales = Sale.objects.create(**content)
            return JsonResponse(
                sales,
                encoder=SaleDetailEncoder,
                safe=False
            )
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Auto vin"},
                status = 400,
            )
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Salesperson does not exist"},
                status = 404,
            )
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Customer does not exist"},
                status = 404,
            )


@require_http_methods(["GET","DELETE"])
def api_show_sale(request,id):
    if request.method == "GET":
        try:
            sales= Sale.objects.get(id=id)
            return JsonResponse(
            sales,
            encoder=SaleDetailEncoder,
            safe=False,
            )
        except Sale.DoesNotExist:
            return JsonResponse(
                {"message": "Does Not Exist"},
                status = 404,
            )
    else:
        count, _ = Sale.objects.filter(id=id).delete()
        if count > 0:
            return JsonResponse({"deleted": count > 0})
        else:
            return JsonResponse(
                {"message": "Does Not Exist"},
                status = 404
            )
