from django.urls import path

from .api_views import (
    api_list_technician,
    api_delete_technician,
    api_list_appointment,
    api_delete_appointment,
    api_finish_appointment,
    api_cancel_appointment,
    api_get_vin,
)


urlpatterns = [
    path("technicians/",
         api_list_technician,
         name="api_list_technician"
    ),
    path(
        "technicians/<int:pk>/",
        api_delete_technician,
        name="api_delete_technician",
    ),
    path(
        "appointments/",
        api_list_appointment,
        name="api_list_appointment",
    ),
    path(
        "appointments/<int:pk>/",
        api_delete_appointment,
        name="api_delete_appointment",
    ),
    path(
        "appointments/<int:pk>/finish/",
        api_finish_appointment,
        name="api_finish_appointment",
    ),
    path(
        "appointments/<int:pk>/cancel/",
        api_cancel_appointment,
        name="api_cancel_appointment",
    ),
    path(
        "vin/",
        api_get_vin,
        name="api_get_vin",
    ),
]
