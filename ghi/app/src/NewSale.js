import React, {useEffect, useState,} from 'react';


function NewSale(){
    const [automobiles, setAutomobiles]= useState([]);
    const [salespeople, setSalesPeople]= useState([]);
    const [customers, setCustomers]= useState([]);
    const [vinNumber, setVinNumber]= useState('');
    const [salesPerson, setSalesPerson]= useState('');
    const [customer, setCustomer]= useState('');
    const [price, setPrice]= useState('');
    const [sold, setSold] = useState(false);

    const fetchData = async () =>{
        const url = 'http://localhost:8100/api/automobiles/';

        const response = await fetch(url);

        if (response.ok) {
        const data1 = await response.json();
        setAutomobiles(data1.autos);
        }
    }
    const fetchSalespeople = async () =>{
        const salesPeopleUrl = 'http://localhost:8090/api/salespeople/';

        const response = await fetch(salesPeopleUrl);

        if (response.ok) {
        const data2 = await response.json();
        setSalesPeople(data2.salespeople);
        }
    }
    const fetchCustomers = async () =>{
        const customerUrl = 'http://localhost:8090/api/customers/';

        const response = await fetch(customerUrl);

        if (response.ok) {
        const data3 = await response.json();
        setCustomers(data3.customers);
        }
    }
    useEffect(() => {fetchData(); fetchSalespeople(); fetchCustomers();}, []);

    const handleSubmit = async (event) => {
        event.preventDefault();
        
        const data={};
        data.automobile = vinNumber;
        data.salesperson = salesPerson;
        data.customer = customer;
        data.price = price;
        
        const cardata={};
        cardata.sold = sold;
        
    const salesUrl = 'http://localhost:8090/api/sales/';

    const fetchConfig = {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        },
    };
    const response = await fetch(salesUrl, fetchConfig);

    const auto = data.automobile;
    const automobilesUrl = `http://localhost:8100/api/automobiles/${auto}/`;
    const fetchSoldConfig= {
        method: "PUT",
        body:JSON.stringify(cardata),
        headers:{
        'Content-Type': 'application/json',
        },
    };
    const soldresponse = await fetch(automobilesUrl, fetchSoldConfig);

    if (response.ok) {
        fetchData();
        fetchSalespeople();
        setAutomobiles([]);
        setSalesPeople([]);
        setCustomer([]);
        setPrice('');
        fetchCustomers();
    }
    }
    const handleAutoVinChange = (event)=> {
        const value = event.target.value;
        setVinNumber(value);
    }
    const handleSalesPersonChange = (event)=> {
        const value = event.target.value;
        setSalesPerson(value);
    }
    const handleCustomerChange = (event)=> {
        const value = event.target.value;
        setCustomer(value);
    }
    const handlePriceChange = (event)=> {
        const value = event.target.value;
        setPrice(value);
    }
    const handleSoldChange = ()=> {
        setSold(true);
    }
    return(
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
            <h1>Record A Sale</h1>
            <form onSubmit={handleSubmit} id="create-sale-form">
            <div className="mb-3">
            <select onChange={handleAutoVinChange} required id="autos" name="autos" value={vinNumber} className="form-select">
                <option >Automobile Vin</option>
                    {automobiles.filter(auto => auto.sold === false).map(auto => {
                        return (
                            <option key={auto.id} value={auto.vin}>
                                {auto.vin}
                            </option>
                        )
                    })}
            </select>
            </div>
            <div className="mb-3">
            <select onChange={handleSalesPersonChange} required id="salesperson" name="salesperson" value={salesPerson} className="form-select">
                <option value='' >Salesperson</option>
                    {salespeople.map(people => {
                        return (
                            <option key={people.id} value={people.employee_id} >
                                {people.first_name}
                            </option>
                        );
                    })}
            </select>
            </div>
            <div className="mb-3">
            <select onChange={handleCustomerChange} required id="customer" name="customer" value={customer} className="form-select">
                <option  >Choose a Customer</option>
                    {customers.map(customer => {
                        return (
                            <option key={customer.id} value={customer.id}>
                                {customer.first_name}
                            </option>
                        )
                    })}
            </select>
            </div>
            <div className="form-floating mb-3">
                <input onChange={handlePriceChange} placeholder="price" required type="text" id="price" name="price" value={price} className="form-control"/>
                <label htmlFor="price">Price</label>
            </div>
            <button onClick={handleSoldChange} className="btn btn-primary me-md-4">Create</button>
            </form>
            </div>
            </div>
        </div>
    )
}

export default NewSale;
