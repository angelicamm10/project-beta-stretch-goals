import React, { useState ,useEffect } from 'react'
function AppointmentHistory() {
    const [appointments, setAppointments] = useState([])
    const [searchQuery, setSearchQuery] = useState('')
    const vinCheck = async (vin) => {
      const url = 'http://localhost:8080/api/vin/'
      const fetchConfig = {
        method: 'get',
        headers: {
          'Content-Type': 'application/json',
        },
      }
      const response = await fetch(url, fetchConfig)
      if (response.ok) {
        const data = await response.json()
        const autos = data.autos
        return autos.some((auto) => vin === auto.vin)
      }
      return false
    }


    const fetchData = async () => {
      const url = 'http://localhost:8080/api/appointments/'
      const response = await fetch(url)
      if (response.ok) {
          const data = await response.json()
          const appointments = data.appointments
          for (const appointment of appointments) {
              const vip = await vinCheck(appointment.vin)
              appointment.isVIP = vip
          }
          setAppointments(appointments)
      }
    }
    useEffect(() => {fetchData()}, [searchQuery])
    return(
        <div>
            <h1>Service History</h1>
            <input type="text" value={searchQuery} onChange={(e) => setSearchQuery(e.target.value)} placeholder="Search by VIN"/>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>VIN</th>
              <th>Is VIP?</th>
              <th>Date</th>
              <th>Time</th>
              <th>Technician</th>
              <th>Reason</th>
              <th>Status</th>
            </tr>
          </thead>
          <tbody>
            {appointments.filter((appointment)=> appointment.vin.includes(searchQuery)).map(appointment => {
              return (
                <tr key={appointment.id}>
                  <td>{appointment.vin}</td>
                  <td>{appointment.isVIP ? "Yes" : "No"}</td>
                  <td>
                    {new Date(appointment.date_time).toLocaleDateString()}
                    </td>
                    <td>
                    {new Date(appointment.date_time).toLocaleTimeString()}
                    </td>
                    <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                    <td>{appointment.reason}</td>
                    <td>{appointment.status.name}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
        </div>
    )
}
export default AppointmentHistory
