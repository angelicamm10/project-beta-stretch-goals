function Footer(){

    return(
        <footer className="footer mt-auto py-5 bg-primary">
            <div className="container-fluid">
                <div className="footer-text">
                    <span className="text-center">info@carcar.com | 555 Drury Lane | 123-456-7890</span>
                </div>

            </div>
            
        </footer>
    )
}

export default Footer;
