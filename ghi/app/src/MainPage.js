import { Link } from 'react-router-dom';
import "./index.css"
function MainPage() {


  return (
  <div>
    <section className={"main-container"}>
      <article className={"main-intro"}>
        <div className="container-fluid">
          <div className={'jumbotron'}>
            <div className={"row"}>
              <div>
              <h1>CarGo</h1>
              <p className="text-break">
              The premiere solution for automobile dealership
              management so you can go,go!
              </p>
              </div>
              <div>
              
              </div>
            </div>
            
          </div>
              
              <div className="px-4  my-5 mt-0 text-center">
                {/* <h1 className="display-10 fw-bold" style ={{fontFamily:"Monserrat Alternates"}}>CarCar</h1> */}
                  <div className="col-lg-6 mx-auto">
                    {/* <p className="lead mb-4">
                        The premiere solution for automobile dealership
                        management so you can go,go!
                    </p> */}
                    {/* <img src="https://img.freepik.com/free-vector/cars-driving-highway-rear-view-automobiles_107791-14428.jpg" style ={{width:"100%"} }className="img-fluid" alt="sebring"/> */}
                  </div> 
              </div>
        </div> 
      </article> 
    </section>  
      <div className="container" >  
        <div className="card mb-3">
          <div className="row g-0">
            <div className="col-md-4">
              <img src="https://img.freepik.com/free-vector/desert-landscape-with-long-highway-cars-ride-along-asphalt-road-with-sign-wires_33099-1493.jpg?w=2000" className="img-fluid rounded-start" alt="sebring"/>
            </div>
          <div className="col-md-8">
          <div className="card-body">
            <h5 className="card-title fw-bold" style ={{fontFamily:"Monserrat Alternates"}}> Manage Inventory</h5>
              <p className="card-text">Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Ut sit amet sagittis dolor, et condimentum lectus. Sed sed varius nisl. Curabitur tempus tempus eleifend. Nulla ac sem eget ex rutrum efficitur sed rhoncus dui. Vestibulum in vehicula purus. Morbi orci ex, laoreet a nunc vel, commodo lobortis ex. Nam iaculis dictum aliquam</p>
              <Link to="/models/list" type="button" className="btn btn-primary btn-sm">Go</Link>
          </div>
            </div>
          </div>
        </div>
      </div>
      <div className="container" >  
        <div className="card mb-3">
          <div className="row g-0">
            <div className="col-md-4">
              <img src="https://img.freepik.com/free-vector/desert-landscape-with-long-highway-cars-ride-along-asphalt-road-with-sign-wires_33099-1493.jpg?w=2000" className="img-fluid rounded-start" alt="sebring"/>
            </div>
          <div className="col-md-8">
          <div className="card-body">
            <h5 className="card-title fw-bold" style ={{fontFamily:"Monserrat Alternates"}}> Manage Services</h5>
              <p className="card-text">Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Ut sit amet sagittis dolor, et condimentum lectus. Sed sed varius nisl. Curabitur tempus tempus eleifend. Nulla ac sem eget ex rutrum efficitur sed rhoncus dui. Vestibulum in vehicula purus. Morbi orci ex, laoreet a nunc vel, commodo lobortis ex. Nam iaculis dictum aliquam</p>
            <Link to="/appointments/list" type="button" className="btn btn-primary btn-sm">Go</Link>
          </div>
            </div>
          </div>
        </div>
      </div>
      <div className="container" >  
        <div className="card mb-3">
          <div className="row g-0">
            <div className="col-md-4">
              <img src="https://img.freepik.com/free-vector/desert-landscape-with-long-highway-cars-ride-along-asphalt-road-with-sign-wires_33099-1493.jpg?w=2000" className="img-fluid rounded-start" alt="sebring"/>
            </div>
          <div className="col-md-8">
          <div className="card-body">
            <h5 className="card-title fw-bold" style ={{fontFamily:"Monserrat Alternates"}}> Manage Sales</h5>
              <p className="card-text">Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Ut sit amet sagittis dolor, et condimentum lectus. Sed sed varius nisl. Curabitur tempus tempus eleifend. Nulla ac sem eget ex rutrum efficitur sed rhoncus dui. Vestibulum in vehicula purus. Morbi orci ex, laoreet a nunc vel, commodo lobortis ex. Nam iaculis dictum aliquam</p>
              <Link to="/sales/list" type="button" className="btn btn-primary btn-sm">Go</Link>
          </div>
            </div>
          </div>
        </div>
      </div>
  </div>   
  )
}

export default MainPage;
